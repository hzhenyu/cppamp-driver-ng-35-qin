#!/bin/bash

# check number of arguments
if [ "$#" -eq 0 ]; then
  echo "$0 is NOT recommended to be directly used" >&2
  exit 1
fi

# tools search priority:
# 1) $HCC_HOME
# 2) @CMAKE_INSTALL_PREFIX@ : default install directory
# 3) @PROJECT_BINARY_DIR@ : build directory

if [ -n "$HCC_HOME" ] && [ -e "$HCC_HOME" ]; then
    LINK=$HCC_HOME/bin/llvm-link
    OPT=$HCC_HOME/bin/opt
    CLAMP_DEVICE=$HCC_HOME/bin/clamp-device
    CLAMP_EMBED=$HCC_HOME/bin/clamp-embed
    HLC_DIR=$HCC_HOME/hlc
    HLC_LLVM_LINK=$HCC_HOME/hlc/bin/llvm-link
    HLC_OPT=$HCC_HOME/hlc/bin/opt
    HLC_LLC=$HCC_HOME/hlc/bin/llc
    HLC_ASM_DIR=$HCC_HOME/HSAILasm
    HLC_ASM=$HCC_HOME/HSAILasm/HSAILasm
elif [ -e @CMAKE_INSTALL_PREFIX@/bin/opt ]; then
    LINK=@CMAKE_INSTALL_PREFIX@/bin/llvm-link
    OPT=@CMAKE_INSTALL_PREFIX@/bin/opt
    CLAMP_DEVICE=@CMAKE_INSTALL_PREFIX@/bin/clamp-device
    CLAMP_EMBED=@CMAKE_INSTALL_PREFIX@/bin/clamp-embed
    HLC_DIR=@CMAKE_INSTALL_PREFIX@/hlc
    HLC_LLVM_LINK=@CMAKE_INSTALL_PREFIX@/hlc/bin/llvm-link
    HLC_OPT=@CMAKE_INSTALL_PREFIX@/hlc/bin/opt
    HLC_LLC=@CMAKE_INSTALL_PREFIX@/hlc/bin/llc
    HLC_ASM_DIR=@CMAKE_INSTALL_PREFIX@/HSAILasm
    HLC_ASM=@CMAKE_INSTALL_PREFIX@/HSAILasm/HSAILasm
elif [ -d @PROJECT_BINARY_DIR@ ]; then
    LINK=@LLVM_TOOLS_DIR@/llvm-link
    OPT=@LLVM_TOOLS_DIR@/opt
    CLAMP_DEVICE=@PROJECT_BINARY_DIR@/lib/clamp-device
    CLAMP_EMBED=@PROJECT_BINARY_DIR@/lib/clamp-embed
    HLC_DIR=@PROJECT_BINARY_DIR@/hlc
    HLC_LLVM_LINK=@PROJECT_BINARY_DIR@/hlc/bin/llvm-link
    HLC_OPT=@PROJECT_BINARY_DIR@/hlc/bin/opt
    HLC_LLC=@PROJECT_BINARY_DIR@/hlc/bin/llc
    HLC_ASM_DIR=@PROJECT_BINARY_DIR@/HSAILasm
    HLC_ASM=@PROJECT_BINARY_DIR@/HSAILasm/HSAILAsm/HSAILasm # notice the "A"
else
    echo "ERROR: Can NOT locate HCC tools! Please specify with $HCC_HOME environmental variable." >&2
    exit 1
fi

if [ -n "@HSA_LLVM_BIN_DIR@" ]; then
    HLC_DIR=@HSA_LLVM_BIN_DIR@
    HLC_LLVM_LINK=@HSA_LLVM_BIN_DIR@/llvm-link
    HLC_OPT=@HSA_LLVM_BIN_DIR@/opt
    HLC_LLC=@HSA_LLVM_BIN_DIR@/llc
fi

################
# Verbose flag
################

VERBOSE=0

################
# GPU targets
################

LOWER_OPENCL=@HAS_OPENCL@
LOWER_HSA=@HAS_HSA@
LOWER_HOF=@HAS_HSA_HOF@

################
# Sanity check for HSAIL backend
################

HSA_USE_AMDGPU_BACKEND=@HSA_USE_AMDGPU_BACKEND@

if [ $HSA_USE_AMDGPU_BACKEND == "ON" ]; then
  KM_USE_AMDGPU="${KM_USE_AMDGPU:=1}"
fi

if [ $LOWER_HSA == 1 ]; then
  if [ -d $HLC_DIR ] && [ -e $HLC_LLVM_LINK ] && [ -e $HLC_OPT ] && [ -e $HLC_LLC ] && [ -d $HLC_ASM_DIR ] && [ -e $HLC_ASM ] || [ $KM_USE_AMDGPU -eq 1 ] ; then
    # all HSA tools are available
    LOWER_HSA=1
  else
    # disable HSA lowering in case any of the required tools is missing
    echo "WARNING: Missing HSA toolchain.  Disable HSA target."
    LOWER_HSA=0
  fi
fi

################
# Sanity check for HSA offline finalization
################

if [ $LOWER_HSA == 1 ] && [ $LOWER_HOF == 1 ]; then
  # Path to HOF
  HOF_BIN=@HOF_BIN@

  if [ -d $HOF_BIN ] && [ -e $HOF_BIN/hof ]; then
    # all HOF tools are available
    HCC_NOISA="${HCC_NOISA:=0}"
    if [ $HCC_NOISA == "1" ]; then
      echo "WARNING: HSA offline finalization explicitely disabled."
      LOWER_HOF=0
    else
      LOWER_HOF=1
    fi
  else
    # disable HOF in case any of the required tools is missing
    echo "WARNING: Missing HOF tools.  Disable HSA offline finalization."
    LOWER_HOF=0
  fi
fi

################
# link
################

LINK_KERNEL_ARGS=""
LINK_HOST_ARGS=""
LINK_OTHER_ARGS=""
LINK_CPU_ARG=""

TEMP_DIR=`mktemp -d`

# a file which contains the list of __cxxamp_serialize symbols in each CPU object file
CXXAMP_SERIALIZE_SYMBOL_FILE=$TEMP_DIR/symbol.txt
touch $CXXAMP_SERIALIZE_SYMBOL_FILE

# find object file
_find_object() {
  local FILE=$1
  local ret=${FILE%.o}

  if [ -e $FILE ]; then
    local file_output=`file $FILE | grep 'ELF 64-bit LSB  relocatable, x86-64'`
    local readelf_output=`readelf -h $FILE | grep 'Relocatable file'`

    if [ ! -z "$file_output" ] && [ ! -z "$readelf_output" ]; then
      # remove postfix
      ret=${FILE%.*}
    fi
  fi

  echo $ret
}

ARGS="$@"
for ARG in $ARGS
do
  if [ -f $ARG ]; then
    FILE=`basename $ARG` # remove path
    FILENAME=$(_find_object $FILE)  # check if it's an object file
    CPUFILE=${FILE%.cpu}
    ISCRT=${ARG#/usr}    # exception for objects under /usr
    ISLIB=${ARG#/lib}    # exception for objects under /lib
    if [ $FILENAME != $FILE ] && [ $ISCRT == $ARG ] && [ $ISLIB == $ARG ]; then
      KERNEL_FILE=$TEMP_DIR/$FILENAME.kernel.bc
      HOST_FILE=$TEMP_DIR/$FILENAME.host.o

      # extract kernel section
      objcopy -O binary -j .kernel $ARG $KERNEL_FILE 

      # extract host section
      objcopy -R .kernel $ARG $HOST_FILE

      # strip all symbols specified in symbol.txt from $HOST_FILE
      objcopy @$CXXAMP_SERIALIZE_SYMBOL_FILE $HOST_FILE $HOST_FILE.new 2> /dev/null
      if [ -f $HOST_FILE.new ]; then
        mv $HOST_FILE.new $HOST_FILE
      fi

      # find cxxamp_serialize symbols and save them into symbol.txt
      objdump -t $HOST_FILE -j .text 2> /dev/null | grep "g.*__cxxamp_serialize" | awk '{print "-L"$6}' >> $CXXAMP_SERIALIZE_SYMBOL_FILE

      LINK_KERNEL_ARGS=$LINK_KERNEL_ARGS" "$KERNEL_FILE
      LINK_HOST_ARGS=$LINK_HOST_ARGS" "$HOST_FILE
    elif [[ $CPUFILE != $FILE ]]; then
        cp $ARG $TEMP_DIR/kernel_cpu.o
        LINK_CPU_ARG=$LINK_CPU_ARG" "$TEMP_DIR/kernel_cpu.o
    else
      LINK_OTHER_ARGS=$LINK_OTHER_ARGS" "$ARG
    fi
  elif [ $ARG == "--verbose" ]; then
    VERBOSE=1
  elif [ $ARG == "--disable-opencl" ]; then
    LOWER_OPENCL=0
  elif [ $ARG == "--disable-hsa" ]; then
    LOWER_HSA=0
  else
    LINK_OTHER_ARGS=$LINK_OTHER_ARGS" "$ARG
  fi
done
#echo "kernel args:"$LINK_KERNEL_ARGS
#echo "host args:"$LINK_HOST_ARGS
#echo "other args:"$LINK_OTHER_ARGS

# linker return value
ret=0

# only do kernel lowering if there are objects given
if [ -n "$LINK_KERNEL_ARGS" ]; then

  # combine kernel sections together
  $LINK $LINK_KERNEL_ARGS | $OPT -always-inline - -o $TEMP_DIR/kernel.bc
  
  # lower to SPIR
  if [ $LOWER_OPENCL == 1 ]; then
    # lower to SPIR
    if [ $VERBOSE == 0 ]; then
      $CLAMP_DEVICE $TEMP_DIR/kernel.bc $TEMP_DIR/kernel.spir --spir
    else
      $CLAMP_DEVICE $TEMP_DIR/kernel.bc $TEMP_DIR/kernel.spir --spir --verbose
    fi
    ret=$?
    if [ $ret == 0 ]; then
      # build a new kernel object
      pushd . > /dev/null
      cd $TEMP_DIR
      $CLAMP_EMBED kernel.spir kernel_spir.o
      popd > /dev/null
    fi
  fi
  
  # lower to OpenCL C
  if [ $LOWER_OPENCL == 1 ]; then
    # lower to OpenCL C
    if [ $VERBOSE == 0 ]; then
      $CLAMP_DEVICE $TEMP_DIR/kernel.bc $TEMP_DIR/kernel.cl --opencl
    else
      $CLAMP_DEVICE $TEMP_DIR/kernel.bc $TEMP_DIR/kernel.cl --opencl --verbose
    fi
    ret=$?
    if [ $ret == 0 ]; then
      # build a new kernel object
      pushd . > /dev/null
      cd $TEMP_DIR
      $CLAMP_EMBED kernel.cl kernel.o
      popd > /dev/null
    fi
  fi
  
  # lower to HSA
  if [ $LOWER_HSA == 1 ]; then
    # lower to HSA BRIG
    if [ $VERBOSE == 0 ]; then
      $CLAMP_DEVICE $TEMP_DIR/kernel.bc $TEMP_DIR/kernel.brig --hsa
    else
      $CLAMP_DEVICE $TEMP_DIR/kernel.bc $TEMP_DIR/kernel.brig --hsa --verbose
    fi
    ret=$?
    if [ $ret == 0 ]; then
      # build a new kernel object
      pushd . > /dev/null
      cd $TEMP_DIR
      $CLAMP_EMBED kernel.brig kernel_hsa.o
      popd > /dev/null
    fi
  fi

  # HSA offline finalization
  if [ $LOWER_HSA == 1 ] && [ $LOWER_HOF == 1 ]; then
    # conduct HSA offline finalization
    $HOF_BIN/hof -output=$TEMP_DIR/kernel.isa -brig $TEMP_DIR/kernel.brig

    ##
    # Lines below are not yet applicable for hof posted on Github
    ##

    # Kaveri
    #HOF_ARCH="AMD:AMDGPU:7:0:0"
     
    # Fiji 
    #HOF_ARCH="AMD:AMDGPU:8:0:3"

    #$HOF_BIN/hof  -target $HOF_ARCH -output $TEMP_DIR/kernel.isa -brig $TEMP_DIR/kernel.brig

    ret=$?
    if [ $ret == 0 ]; then
      # build a new kernel object
      pushd . > /dev/null
      cd $TEMP_DIR
      $CLAMP_EMBED kernel.isa kernel_hof.o
      popd > /dev/null

      # debug purpose
      # dump the brig
      KMDUMPBRIG="${KMDUMPBRIG:=0}"
      if [ $KMDUMPBRIG == "1" ]; then
        cp $TEMP_DIR/kernel.isa ./dump.isa
      fi
    fi
  fi
  
  if [ $ret == 0 ]; then
    # link everything together
    if [ $LOWER_OPENCL == 1 ] && [ $LOWER_HSA == 1 ] && [ $LOWER_HOF == 1 ]; then
      ld --allow-multiple-definition $TEMP_DIR/kernel.o $TEMP_DIR/kernel_spir.o $TEMP_DIR/kernel_hsa.o $TEMP_DIR/kernel_hof.o $LINK_HOST_ARGS $LINK_CPU_ARG $LINK_OTHER_ARGS
    elif [ $LOWER_OPENCL == 1 ] && [ $LOWER_HSA == 1 ] && [ $LOWER_HOF == 0 ]; then
      ld --allow-multiple-definition $TEMP_DIR/kernel.o $TEMP_DIR/kernel_spir.o $TEMP_DIR/kernel_hsa.o $LINK_HOST_ARGS  $LINK_CPU_ARG $LINK_OTHER_ARGS
      ret=$?
    elif [ $LOWER_OPENCL == 1 ] && [ $LOWER_HSA == 0 ]; then
      ld --allow-multiple-definition $TEMP_DIR/kernel.o $TEMP_DIR/kernel_spir.o $LINK_HOST_ARGS $LINK_CPU_ARG $LINK_OTHER_ARGS
      ret=$?
    elif [ $LOWER_OPENCL == 0 ] && [ $LOWER_HSA == 1 ] && [ $LOWER_HOF == 1 ]; then
      ld --allow-multiple-definition $TEMP_DIR/kernel_hsa.o $TEMP_DIR/kernel_hof.o $LINK_HOST_ARGS $LINK_CPU_ARG $LINK_OTHER_ARGS
    elif [ $LOWER_OPENCL == 0 ] && [ $LOWER_HSA == 1 ] && [ $LOWER_HOF == 0 ]; then
      ld --allow-multiple-definition $TEMP_DIR/kernel_hsa.o $LINK_HOST_ARGS $LINK_CPU_ARG $LINK_OTHER_ARGS
      ret=$?
    else
      echo "ERROR: No GPU target available! Linker failed."
      ret=1
    fi
  fi

fi # if [ -n $LINK_KERNEL_ARGS ];

# remove temp files
if [ -e $TEMP_DIR/kernel.o ]; then
  rm $TEMP_DIR/kernel.o
fi

if [ -e $TEMP_DIR/kernel_spir.o ]; then
  rm $TEMP_DIR/kernel_spir.o
fi

if [ -e $TEMP_DIR/kernel_hof.o ]; then
  rm $TEMP_DIR/kernel_hof.o
fi

if [ -e $TEMP_DIR/kernel_hsa.o ]; then
  rm $TEMP_DIR/kernel_hsa.o
fi

if [ -e $TEMP_DIR/kernel_cpu.o ]; then
  rm $TEMP_DIR/kernel_cpu.o
fi

if [ -e $TEMP_DIR/kernel.cl ]; then
  rm $TEMP_DIR/kernel.bc
fi

if [ -e $TEMP_DIR/kernel.spir ]; then
  rm $TEMP_DIR/kernel.spir
fi

if [ -e $TEMP_DIR/kernel.isa ]; then
  rm $TEMP_DIR/kernel.isa
fi

if [ -e $TEMP_DIR/kernel.brig ]; then
  rm $TEMP_DIR/kernel.brig
fi

if [ -e $TEMP_DIR/kernel.bc ]; then
  rm $TEMP_DIR/kernel.bc
fi

if [ -n "$LINK_KERNEL_ARGS" ]; then
  rm $LINK_KERNEL_ARGS # individual kernels
fi

if [ -n "$LINK_HOST_ARGS" ]; then
  rm $LINK_HOST_ARGS # individual host codes
fi

if [ -e $CXXAMP_SERIALIZE_SYMBOL_FILE ]; then
  rm $CXXAMP_SERIALIZE_SYMBOL_FILE # __cxxamp_serialize symbols
fi

if [ -d $TEMP_DIR ]; then
  rm -f $TEMP_DIR/*
  rmdir $TEMP_DIR
fi

# return value
exit $ret
