#!/bin/bash

# check command line arguments
if [ "$#" -ne 2 ]; then
  echo "Usage: $0 input_object output_kernel" >&2
  exit 1
fi

if [ ! -f $1 ]; then
  echo "input object $1 is not valid" >&2
  exit 1
fi

# tools search priority:
# 1) $HCC_HOME
# 2) @CMAKE_INSTALL_PREFIX@ : default install directory
# 3) @LLVM_TOOLS_DIR@ : build directory

if [ -n "$HCC_HOME" ] && [ -e "$HCC_HOME" ]; then
    OPT=$HCC_HOME/bin/opt
    LLC=$HCC_HOME/bin/llc
    LIB=$HCC_HOME/lib
elif [ -e @CMAKE_INSTALL_PREFIX@ ]; then
    OPT=@CMAKE_INSTALL_PREFIX@/bin/opt
    LLC=@CMAKE_INSTALL_PREFIX@/bin/llc
    LIB=@CMAKE_INSTALL_PREFIX@/lib
elif [ -d @LLVM_TOOLS_DIR@ ]; then
    OPT=@LLVM_TOOLS_DIR@/opt
    LLC=@LLVM_TOOLS_DIR@/llc
    LIB=@LLVM_LIBS_DIR@
else
    echo "ERROR: Can NOT locate HCC tools! Please specify with $HCC_HOME environmental variable." >&2
    exit 1
fi

if [[ $OSTYPE == linux-gnu ]]; then
    FILE=`basename $1`
    FILENAME=${FILE%cpu}
    if [[ $FILE != $FILENAME ]]; then
        $OPT -load $LIB/LLVMCpuRename@CMAKE_SHARED_LIBRARY_SUFFIX@ \
                -cpu-rename $1 -S -o $1.S
        $LLC -O=2 $1.S -relocation-model=pic -filetype=obj -o $2
        rm $1.S
    else
        objcopy -B i386:x86-64 -I binary -O elf64-x86-64 --rename-section .data=.kernel $1 $2
    fi
elif [[ $OSTYPE == darwin* ]]; then
# OS X
  touch $2.stub.c
  gcc -o $2.stub.o -c $2.stub.c
  ld -r -o $2 -sectcreate binary kernel_cl $1 $2.stub.o
  rm -f $2.stub.c $2.stub.o
fi
